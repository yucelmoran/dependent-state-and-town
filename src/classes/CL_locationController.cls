/**
 *	Summary: 	Class that allow users do a dependent pick-list between 
 			 	state and town.
 *	Developer:	Edgar Yucel Moran (yucel.moran@gmail.com)
 *	Date: 		Feebruary 12 2014.
 */
public class CL_locationController {
	
	public String state {get;set;}
    public String town {get;set;}
	
	private final Account acct;
	 
	/*
		Constructor
	*/
    public CL_locationController(ApexPages.StandardController stdController) {
        this.acct = (Account)stdController.getRecord();
        System.debug('## Account Information: '+this.acct);
    }
    
	/*
		Get States
	*/
    public List<SelectOption> getStates()
    {
    	List<SelectOption> options = new List<SelectOption>();
    	options.add(new SelectOption('-Selecciona-','-Selecciona-'));       
    	for(Estado_Municipio__c edomun:[Select Id,Name,Tipo__c from Estado_Municipio__c where Tipo__c='Estado' order By Name ASC]){	        
	        options.add(new SelectOption(edomun.Id,edomun.Name));        
    	}
        
        return options;
    }
    
    /*
		Get Town
	*/
    public List<SelectOption> getTowns()
    {
    	List<SelectOption> options = new List<SelectOption>();
    	options.add(new SelectOption('-Selecciona-','-Selecciona-')); 
    	
    	System.debug('EDGAR MORAN: '+state);
    	if(this.state!=null || this.state!='-Selecciona-'){      
	    	for(Estado_Municipio__c edomun:[Select Id,Name,Tipo__c,Estado__r.Name from Estado_Municipio__c where Tipo__c='Municipio' and Estado__c =:this.state order By Name ASC]){	        
		        
		        options.add(new SelectOption(edomun.Id,edomun.Name));        
	    	}
    	}
        
        return options;
    }
	
	
	
    
}