/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_CL_LocationController {

    static testMethod void myUnitTest() {
		
		RecordType rt=[Select Id,Name from RecordType where Name='Registro Cliente'];
       
     	
        Account a = new Account(name='Tester');
       	a.RecordTypeId=rt.Id;
	    insert a;
	    
	    Estado_Municipio__c edo=new Estado_Municipio__c();
	    edo.Tipo__c = 'Estado';
	    edo.Name='Estado de Mexico';
	    
	    insert edo;
	    
	    Estado_Municipio__c mun=new Estado_Municipio__c();
	    mun.Name='Tecamac';
	    mun.Tipo__c = 'Municipios';
	    mun.Estado__c = edo.Id;
	    
	    insert mun;
	    
	    ApexPages.StandardController sc = new ApexPages.standardController(a);
	    CL_locationController e = new CL_locationController(sc);
	    
	    List<SelectOption> lo=e.getStates();
	    List<SelectOption> lo2=e.getTowns();
	    
	    e.state = edo.Id;
	    //System.assertEquals(e.acct, a);
    }
}